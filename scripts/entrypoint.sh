#!/bin/sh

set -e

#noinput = -y for not interaction
python manage.py collectstatic --noinput
#make sure db is up before proceeding
python manage.py wait_for_db
#using migrate cmd to upate db
python manage.py migrate

#uwsgi cmd passing in socket
#set up workers
#run as master on terminal
#enable multithreading
#module is app that uwsgi is going to run
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi